import React, { useState, useEffect } from "react"
import { Image, View, Alert } from "react-native"
import { Body, Item, Input, Picker, Button, Card, CardItem, Col, Container, Content, Footer, FooterTab, Grid, H3, Header, Icon, Left, List, ListItem, Right, Text, Thumbnail, Title } from "native-base"
import { ScrollView } from "react-native-gesture-handler"
import { useSelector, useDispatch } from "react-redux"
import styles from "./styles"
import { get, put } from "../../helpers/fetch"
import { root_setUser } from "../../stores/root.action"

const wahyu = require("../../assets/img/author.jpg")

const MyProfile = ({ navigation }) => {
	const { user } = useSelector(state => state.root)
	const dispatch = useDispatch()

	const [form, setForm] = useState(user)
	const [jabatan, setJabatan] = useState([])
	const handleForm = (name, value) => setForm({ ...form, [name]: value })

	const handleUpdate = () => {
		alert("masuk" + JSON.stringify(form))
		put(`user/${form.id}`, form).then(res => {
			alert("masuk2" + JSON.stringify(res))
			if (res.status === "success") {
				dispatch(root_setUser(form))
				Alert.alert("Berhasil", "Profil berhasil diupdate.")
			} else {				
				Alert.alert("Gagal", "Profil gagal diupdate, coba lagi.")
			}
		})
		.catch(err => Alert.alert("Error", JSON.stringify(err)))
	}

	useEffect(() => {
		get(`jabatan`).then(res => {
			if (res.status === "success") {
				setJabatan(res.data)
			} else {
				Alert.alert("Data kosong", "Data jabatan kosong.")
			}
		})
	}, [])

	return (
		<Container style={styles.container}>
			<Header transparent androidStatusBarColor="#fff">
				<Left>
					<Image style={{ height: 30 }} source={require("../../assets/img/logo-covid-header.png")} />
				</Left>
				<Body>
					<Title style={{ color: "#4F4F4F", marginLeft: 52 }}>My Profile</Title>
				</Body>
				<Right />
			</Header>

			<ScrollView>
				<Content padder>
					<Card>
						<View style={{ justifyContent: "center", alignItems: "center", backgroundColor: "#0486FE" }}>
							<Thumbnail large style={{ marginTop: 16 }} source={wahyu} />
							<Text style={{ color: "white", fontSize: 18, marginTop: 16 }}>{user.fullname}</Text>
						</View>

						<CardItem style={{ backgroundColor: "#0486FE" }}>
							<Grid style={{ marginBottom: 16 }}>
								<Col>
									<Button block style={{
										backgroundColor: "#0486FE",
										marginRight: 8,
										shadowColor: 'rgba(0, 0, 0, 0.1)',
										shadowOpacity: 0.8,
										elevation: 6,
										shadowRadius: 15,
										shadowOffset: { width: 1, height: 13 }
									}}>
										<Text>Data Personal</Text>
									</Button>
								</Col>
								<Col>
									<Button
										block
										onPress={() => navigation.navigate("MyProfileKeamanan")}
										style={{
											backgroundColor: "#294F74",
											marginLeft: 8,
											shadowColor: 'rgba(0, 0, 0, 0.1)',
											shadowOpacity: 0.8,
											elevation: 6,
											shadowRadius: 15,
											shadowOffset: { width: 1, height: 13 }
										}}>
										<Text>Keamanan</Text>
									</Button>
								</Col>
							</Grid>
						</CardItem>
					</Card>

					<List>
						<Card>
							<CardItem header>
								<H3 style={{ color: "#FEAC0E" }}>Personal Data</H3>
							</CardItem>

							<ListItem>
								<Left>

									<Body style={{ marginLeft: 0 }}>
										<Text note>Nama</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon style={{ color: "#FEAC0E" }} name='user' type="AntDesign" />
											<Input value={form.fullname} onChangeText={v => handleForm("fullname", v)} placeholder="Tolong input nama Anda" />
										</Item>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>

									<Body style={{ marginLeft: 0 }}>
										<Text note>Jabatan</Text>
										<Item regular picker>
												<Picker mode="dropdown" iosIcon={<Icon name="arrow-down" />}
													style={{ width: undefined }}
													placeholder="Lorem Ipsum"
													placeholderStyle={{ color: "#bfc6ea" }}
													placeholderIconColor="#007aff"
													selectedValue={form.id_jabatan}
													onValueChange={v => handleForm("id_jabatan", v)}
												>
													<Picker.Item value="" label="-" />
													{jabatan.map(x => (
														<Picker.Item key={x.id} value={`${x.id}`} label={x.name} />
													))}
												</Picker>
											</Item>
									</Body>
								</Left>
							</ListItem>
							<ListItem>
								<Left>

									<Body style={{ marginLeft: 0 }}>
										<Text note>No. Tlp</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon active style={{ color: "#FEAC0E" }} name='phone' type="Entypo" />
											<Input value={form.no_telepon} onChangeText={v => handleForm("no_telepon", v)} placeholder="Nomor telephone" />
										</Item>
									</Body>
								</Left>
							</ListItem>
							<ListItem>
								<Left>

									<Body style={{ marginLeft: 0 }}>
										<Text note>Alamat</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon active style={{ color: "#FEAC0E" }} name='location-city' type="MaterialIcons" />
											<Input value={form.address} onChangeText={v => handleForm("address", v)} placeholder="Nama Alamat" />
										</Item>
									</Body>
								</Left>
							</ListItem>

						</Card>
					</List>

					<Button
						onPress={handleUpdate}
						block
						style={{ backgroundColor: "#0677F9", marginTop: 16 }}>
						<Text>EDIT</Text>
					</Button>

				</Content>
			</ScrollView>

			<Footer>
				<FooterTab style={{ backgroundColor: "white" }}>
					<Button
						vertical
						style={{ backgroundColor: "white" }}
						onPress={() => navigation.navigate("Dashboard")}>
						<Icon style={{ color: "grey" }} type="AntDesign" name="home" />
						<Text style={{ color: "grey", fontSize: 9 }}>Home</Text>
					</Button>
					<Button vertical style={{ backgroundColor: "white" }} >
						<Icon style={{ color: "grey" }} type="AntDesign" name="notification" />
						<Text style={{ fontSize: 9 }}>Reminder</Text>
					</Button>
					<Button
						vertical
						style={{ backgroundColor: "white" }}
						onPress={() => navigation.navigate("ScreeningListView")}>
						<Icon style={{ color: "grey" }} type="AntDesign" name="solution1" />
						<Text style={{ fontSize: 9 }}>Screening</Text>
					</Button>
					<Button
						vertical
						style={{ backgroundColor: "white" }}
						onPress={() => navigation.navigate("MyProfile")}>
						<Icon style={{ color: "#FEAC0E" }} type="AntDesign" name="user" />
						<Text style={{ fontSize: 9, color: "#FEAC0E" }}>Profile</Text>
					</Button>
				</FooterTab>
			</Footer>
		</Container>
	)
}

export default MyProfile
