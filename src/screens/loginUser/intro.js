import React, { useEffect } from "react"
import { Dimensions, Image, View } from "react-native"
import { Button, Container, Content, Header, Text, H1, H2, H3 } from "native-base"
import { useSelector, useDispatch } from "react-redux"
import styles from "./styles"
import moment from "moment"
import "moment/locale/id"

import { post } from "../../helpers/fetch"
import { root_setNegara, root_setProvinsi } from "../../stores/root.action"


const deviceWidth = Dimensions.get("window").width

const Intro = ({ navigation }) => {
	const { user, negara, provinsi } = useSelector(state => state.root)
	const dispatch = useDispatch()

	const today = moment(new Date())

	const getHumanTime = () => {
		const now = new Date()
		const hour = now.getHours()
		switch (true) {
		  case hour < 12:
			return "Selamat Pagi";
		  case hour < 15:
			return "Selamat Siang";
		  case hour < 18:
			return "Selamat Sore";
		  default:
			return "Selamat Malam";
		}
	}
	const handleLongPress = () => {
		navigation.navigate("ConfigApi")
	}
	
	useEffect(() => {
		post(`http://ems-service.jatimprov.smartcovid19.com/api/get-negara`).then(resNegara => {
			if (resNegara.success) {
				dispatch(root_setNegara(resNegara.data))
				post(`http://ems-service.jatimprov.smartcovid19.com/api/get-prop`).then(resProvinsi => {
					if (resProvinsi.success) {
						dispatch(root_setProvinsi(resProvinsi.data))						
					}
				})
			}
		})
	}, [])

	return (
		<Container style={styles.container}>
			<Header transparent style={{ height: 0 }} androidStatusBarColor="#fff" />

			<Image source={require("../../assets/img/background-forgot-password.jpg")}
				style={{ height: 300, width: deviceWidth, flex: 1 }} />

			<View style={{ position: "absolute", left: 8, right: 8, top: 150 }}>
				<Content padder>

					<View style={{ alignItems: "center" }}>
						<H2 style={{ color: "#fff" }}>{today.format("dddd")}</H2>
						<H3 style={{ color: "#fff", marginTop: 8 }}>{today.format("DD MMMM YYYY")}</H3>
					</View>

					<View style={{ alignItems: "center", marginTop: 24 }}>
						<H2 style={{ fontWeight: "100", color: "#fff" }}>{getHumanTime()}</H2>
						<H3 style={{ fontWeight: "bold", color: "#fff", marginTop: 8 }}>{user.fullname}</H3>
					</View>

					<View style={{ alignItems: "center", marginTop: 24 }}>
						<Text onLongPress={handleLongPress} style={{ color: "#fff" }}>Selamat bertugas</Text>
						<Text style={{ color: "#fff", marginTop: 8 }}>Selalu jaga kesehatan</Text>
					</View>


					<Button
						block
						rounded
						style={{ marginTop: 42, backgroundColor: "#FEAC0E" }}
						onPress={() => navigation.navigate("Dashboard", { user })}
					>
						<Text>LANJUT</Text>
					</Button>

				</Content>
			</View>
		</Container>
	)
}

export default Intro
