import React from "react"
import { Image, View } from "react-native"
import { Button, Card, CardItem, Container, Content, Footer, FooterTab, Header, Icon, Text } from "native-base"
import { ScrollView } from "react-native-gesture-handler"
import styles from "./styles"

const cardImage04 = require("../../../assets/tulungagung.jpg")

const Dashboard = ({ navigation }) => {

	return (
		<Container style={{ backgroundColor: "#4A58A1" }}>
			<Header style={{ height: 0 }} transparent androidStatusBarColor="#fff" />

			<ScrollView >
				<Content>
					<Card style={{ marginTop: 0, marginLeft: 0, marginRight: 0 }}>
						<CardItem cardBody style={{ position: "relative" }}>
							<Image source={cardImage04} style={{ height: 300, width: null, flex: 1, resizeMode: 'contain' }} />
						</CardItem>
						<Image style={{ position: "absolute", left: 18, top: 18 }} source={require("../../assets/img/logo-covid-header.png")} />
					</Card>

					<Button
						// onPress={() => navigation.navigate("ProcessScreeningOne")}ScreenReact
						onPress={() => navigation.navigate("ProcessScreeningOne")}
						style={{ alignSelf: "center", justifyContent: 'center', alignItems: 'center', marginVertical: 10, borderRadius: 100, width: 150, height: 150, borderWidth: 10, borderColor: "#fff", backgroundColor: "#4A58A1" }}>
						<Text style={{ fontSize: 16, color: "#f44336" }}>SCREENING</Text> 
					</Button>

					<View style={{ alignSelf: "center" }}>
						<Text style={{ color: "#fff" }}>Lakukan Screening Sekarang</Text>
					</View>

				</Content>
			</ScrollView>

			<Footer>
				<FooterTab style={{ backgroundColor: "white" }}>
					<Button vertical style={{ backgroundColor: "white" }}  >
						<Icon style={{ color: "#FEAC0E" }} type="AntDesign" name="home" />
						<Text style={{ color: "#FEAC0E", fontSize: 9 }}>Home</Text>
					</Button>
					<Button vertical style={{ backgroundColor: "white" }} >
						<Icon style={{ color: "grey" }} type="AntDesign" name="notification" />
						<Text style={{ fontSize: 9 }}>Reminder</Text>
					</Button>
					<Button
						vertical
						style={{ backgroundColor: "white" }}
						onPress={() => navigation.navigate("ScreeningListView")}>
						<Icon style={{ color: "grey" }} type="AntDesign" name="solution1" />
						<Text style={{ fontSize: 9 }}>Screening</Text>
					</Button>
					<Button
						vertical
						style={{ backgroundColor: "white" }}
						onPress={() => navigation.navigate("MyProfile")}>
						<Icon style={{ color: "grey" }} type="AntDesign" name="user" />
						<Text style={{ fontSize: 9 }}>Profile</Text>
					</Button>
				</FooterTab>
			</Footer>
		</Container>
	)
}

export default Dashboard
