import React from "react"
import { Alert } from "react-native"
import { Body, Switch, Button, Card, CardItem, Container, Content, H3, Header, Icon, Input, Item, Left, List, ListItem, Right, Text, Badge } from "native-base"
import { ScrollView } from "react-native-gesture-handler"
import { useSelector, useDispatch } from "react-redux"
import styles from "./styles"

import { screening_handleFormKontakCepat } from "../../stores/screening.action"

const ProcessScreeningCepat = ({ navigation }) => {
	const { formkontakcepat } = useSelector(state => state.screening)
	const dispatch = useDispatch()

	const handleForm = (name, value) => dispatch(screening_handleFormKontakCepat(name, value))
	const handleEnterNoHp = e => {					
		if(e.nativeEvent.key === " " && formkontakcepat.isWa){
			Alert.alert("Konfirmasi", "Apakah No. Whatsapp (WA) anda sama dengan No. Telepon atau voice contact ?", [
				{text: "Sama", onPress: () => {
					handleForm("telp_no", formkontakcepat.wa_no)
				}},
				{text: "Berbeda", onPress: () => {}},
			])
		}
	}

	const handleCheckWa = () => {
		if (formkontakcepat.isWa) {
			dispatch(screening_handleFormKontakCepat("wa_no", ""))
		}
		dispatch(screening_handleFormKontakCepat("isWa", !formkontakcepat.isWa))
	}
	
	const handleCheckTelp = () => {
		if (formkontakcepat.isTelp) {
			dispatch(screening_handleFormKontakCepat("telp_no", ""))
		}
		dispatch(screening_handleFormKontakCepat("isTelp", !formkontakcepat.isTelp))
	}

	const handleNext = () => {
		const { isWa, isTelp, wa_no, telp_no } = formkontakcepat
		if ((isWa && wa_no !== "") ? true : (isTelp && telp_no !== "")) {
			navigation.navigate("ProcessScreeningTwo")
		} else {
			if (isWa && wa_no === "") {
				Alert.alert("No. Whatsapp Kosong", "Silahkan lengkapi No. WA sebelum melanjutkan.")
			} else if (!isWa && isTelp && telp_no === "") {
				Alert.alert("No. Telepon belum lengkap", "Silahkan lengkapi No. Telepon Pribadi (karena tanpa No. WA) sebelum melanjutkan.")				
			} else if (!isWa && !isTelp && wa_no === "" && telp_no === "") {
				Alert.alert("No. Kontak belum lengkap", "Silahkan lengkapi No. WA / No. Telepon Pribadi sebelum melanjutkan.")
			} else {
				Alert.alert("Identitas belum lengkap", "Silahkan lengkapi data identitas sebelum melanjutkan.")
			}
		}
	}

	return (
		<Container style={styles.container}>
			<Header transparent androidStatusBarColor="#fff">
				<Left>
					<Button transparent onPress={() => navigation.navigate("ProcessScreeningOne")}>
						<Icon style={{ color: "#4F4F4F" }} name="md-arrow-back" />
					</Button>
				</Left>
				<Body>
					<Text style={{ color: "#4F4F4F" }}>Process Screening</Text>
				</Body>
				<Right />
			</Header>

			<ScrollView style={{ marginTop: 8 }} >
				<Content padder>

					<List>
						<Card>

							<CardItem header>
								<H3 style={{ color: "#FEAC0E" }}>Nomor Kontak Warga</H3>
							</CardItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>No Telp WA</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon style={{ color: "#FEAC0E" }} name='whatsapp' type="FontAwesome" />
											<Input keyboardType="phone-pad" value={formkontakcepat.wa_no} onChangeText={v => handleForm("wa_no", v)} onKeyPress={handleEnterNoHp} disabled={!formkontakcepat.isWa} placeholder="Input No. WA" />
										</Item>
									</Body>
								</Left>
								<Right>
									<Text />
									<Badge style={{backgroundColor: "#0677F9", marginBottom: 5 }}>
										<Text style={{fontSize: 12}}>
											{formkontakcepat.isWa ? "Ya" : "Tidak"}
										</Text>
									</Badge>
									<Switch style={{marginBottom: 4}} value={formkontakcepat.isWa} onValueChange={handleCheckWa} />
								</Right>
							</ListItem>
							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>No. Telp Pribadi (Voice Contact)</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon style={{ color: "#FEAC0E" }} name='whatsapp' type="FontAwesome" />
											<Input keyboardType="phone-pad" value={formkontakcepat.telp_no} onChangeText={v => handleForm("telp_no", v)} disabled={!formkontakcepat.isTelp} placeholder="Input No. Telp Pribadi" />
										</Item>
									</Body>
								</Left>
								<Right>
									<Text />
									<Badge style={{backgroundColor: "#0677F9", marginBottom: 5 }}>
										<Text style={{fontSize: 12}}>
											{formkontakcepat.isTelp ? "Ya" : "Tidak"}
										</Text>
									</Badge>
									<Switch value={formkontakcepat.isTelp} onValueChange={handleCheckTelp} style={{marginBottom: 4}} />
								</Right>
							</ListItem>	
								
						</Card>
					</List>

					<Button 
						onPress={handleNext} 
						block 
						style={{ backgroundColor: "#0677F9", marginTop: 16 }}>
						<Text>NEXT</Text>
					</Button>

				</Content>
			</ScrollView>
		</Container>
	)
}

export default ProcessScreeningCepat
