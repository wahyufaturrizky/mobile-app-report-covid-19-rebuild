import { Body, Button, Card, CardItem, Container, Content, Header, Icon, Left, List, ListItem, Right, Text } from "native-base"
import React from "react"
import { View } from "react-native"
import { ScrollView } from "react-native-gesture-handler"
import styles from "./styles"


const ProcessScreeningSeven = ({ navigation }) => {

	return (
		<Container style={styles.container}>
			<Header transparent androidStatusBarColor="#fff">
				<Left>
					<Button transparent onPress={() => navigation.goBack()}>
						<Icon style={{ color: "#4F4F4F" }} name="md-arrow-back" />
					</Button>
				</Left>
				<Body>
					<Text style={{ color: "#4F4F4F" }}>Informasi Faskes</Text>
				</Body>
				<Right />
			</Header>

			<ScrollView style={{ marginTop: 8 }} >
				<Content padder>

					<List>
						<Card>
							<CardItem header>
								<Text style={{ color: "#4F4F4F", fontSize: 14, fontWeight: "normal" }}>LOKASI DAN JADWAL FASKES KHUSUS DARURAT COVID</Text>
							</CardItem>

							{/*  */}

							<CardItem>
								<Text style={{ color: "#4F4F4F", fontSize: 12, fontWeight: "normal" }}>KABUPATEN TULUNG AGUNG</Text>
							</CardItem>

							<CardItem style={{ marginBottom: 0 }}>
								<Text style={{ color: "#4F4F4F", fontSize: 11, fontWeight: "normal" }}>Ada 4 faskes khusus darurat Covid-19 :</Text>
							</CardItem>

							<ListItem noBorder style={{ paddingTop: 0 }}>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text style={{ color: "#4F4F4F", fontSize: 11, fontWeight: "normal" }}>1. PUSKESMAS NGANTRU</Text>
										<View style={{ marginLeft: 10, marginTop: 4 }}>
											<Text style={{ color: "#4F4F4F", fontSize: 11, fontWeight: "normal" }}>a. Jam periksa: 07:00 - 14:00</Text>
											<Text style={{ color: "#4F4F4F", fontSize: 11, fontWeight: "normal", marginTop: 4 }}>b. Hari Senin s/d Jum'ats</Text>
										</View>
									</Body>
								</Left>
							</ListItem>
							<ListItem noBorder style={{ paddingTop: 0 }}>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text style={{ color: "#4F4F4F", fontSize: 11, fontWeight: "normal" }}>2. PUSKESMAS LOREM IPSUM</Text>
										<View style={{ marginLeft: 10, marginTop: 4 }}>
											<Text style={{ color: "#4F4F4F", fontSize: 11, fontWeight: "normal" }}>a. Jam periksa: 07:00 - 14:00</Text>
											<Text style={{ color: "#4F4F4F", fontSize: 11, fontWeight: "normal", marginTop: 4 }}>b. Hari Senin s/d Jum'ats</Text>
										</View>
									</Body>
								</Left>
							</ListItem>

							{/*  */}

							<CardItem style={{ marginBottom: 0 }}>
								<Text style={{ color: "#4F4F4F", fontSize: 11, fontWeight: "normal" }}>Prosedur daftar pemeriksaan :</Text>
							</CardItem>

							<ListItem noBorder style={{ paddingTop: 0 }}>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<View style={{ marginLeft: 10 }}>
											<Text style={{ color: "#4F4F4F", fontSize: 11, fontWeight: "normal" }}>1. Lorem Ipsum</Text>
											<Text style={{ color: "#4F4F4F", fontSize: 11, fontWeight: "normal", marginTop: 4 }}>2. Lorem Ipsum</Text>
										</View>
									</Body>
								</Left>
							</ListItem>

							{/*  */}

							<ListItem noBorder style={{ paddingTop: 0 }}>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text style={{ color: "#4F4F4F", fontSize: 11, fontWeight: "normal" }}>Catatan : Lorem Ipsum</Text>
									</Body>
								</Left>
							</ListItem>

							{/*  */}

							<ListItem noBorder style={{ paddingTop: 0 }}>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text style={{ color: "#4F4F4F", fontSize: 11, fontWeight: "normal" }}>Mari kita melakukan hal yang perlu, sehingga bisa saling menjaga kesehatan dan keselamatan setiap orang dan orang - orang yang kita kasihi. Terimakasih</Text>
									</Body>
								</Left>
							</ListItem>


						</Card>
					</List>

					<Button
						block
						style={{ backgroundColor: "#0677F9", marginTop: 16 }}
						onPress={() => navigation.goBack()}>
						<Text>KIRIM WA</Text>
					</Button>

				</Content>
			</ScrollView>
		</Container>
	)
}

export default ProcessScreeningSeven
