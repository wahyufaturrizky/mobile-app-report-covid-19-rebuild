import React from "react"
import { Alert } from "react-native"
import { Body, Switch, Form, Picker, Button, Card, Badge, CardItem, Container, Content, H3, Header, Icon, Input, Item, Left, List, ListItem, Right, Text } from "native-base"
import { ScrollView } from "react-native-gesture-handler"
import { useDispatch, useSelector } from "react-redux"
import styles from "./styles"

import { post } from "../../helpers/fetch"
import ucfirst from "../../helpers/ucfirst"
import { root_setKabupaten, root_setKecamatan, root_setKelurahan } from "../../stores/root.action"
import { screening_handleFormDomisili } from "../../stores/screening.action"


const ProcessScreeningDataLokasiSaatIni = ({ navigation }) => {
	const { root: { provinsi, kabupaten, kecamatan, kelurahan }, screening: { formdomisili } } = useSelector(state => state)
	const dispatch = useDispatch()



	const handleForm = (name, value) => dispatch(screening_handleFormDomisili(name, value))

	const handleChangeProvinsi = (name, id) => {
		handleForm(name, id)
		post(`http://ems-service.jatimprov.smartcovid19.com/api/get-kab`, { id }).then(resKabupaten => {
			if (resKabupaten.success) {
				dispatch(root_setKabupaten(resKabupaten.data))
			}
		})
	}

	const handleChangeKabupaten = (name, id) => {
		handleForm(name, id)
		post(`http://ems-service.jatimprov.smartcovid19.com/api/get-kec`, { id }).then(resKecamatan => {
			if (resKecamatan.success) {
				dispatch(root_setKecamatan(resKecamatan.data))
			}
		})
	}

	const handleChangeKecamatan = (name, id) => {
		handleForm(name, id)
		post(`http://ems-service.jatimprov.smartcovid19.com/api/get-kel`, { id }).then(resKelurahan => {
			if (resKelurahan.success) {
				dispatch(root_setKelurahan(resKelurahan.data))
			}
		})
	}

	const handleEnterNoHp = e => {			
		if(e.nativeEvent.key === " " && formdomisili.keluarga_isWa){
			Alert.alert("Konfirmasi", "Apakah No. Whatsapp (WA) keluarga dekat anda sama dengan No. Telepon atau voice contact ?", [
				{text: "Sama", onPress: () => {
					handleForm("keluarga_telp_no", formdomisili.keluarga_wa_no)
				}},
				{text: "Berbeda", onPress: () => {}},
			])
		}
	}

	const handleCheckWa = () => {
		if (formdomisili.keluarga_isWa) {
			dispatch(screening_handleFormDomisili("keluarga_wa_no", ""))
		}
		dispatch(screening_handleFormDomisili("keluarga_isWa", !formdomisili.keluarga_isWa))
	}
	
	const handleCheckTelp = () => {
		if (formdomisili.keluarga_isTelp) {
			dispatch(screening_handleFormDomisili("keluarga_telp_no", ""))
		}
		dispatch(screening_handleFormDomisili("keluarga_isTelp", !formdomisili.keluarga_isTelp))
	}

	const handleBack = () => {
		navigation.navigate("ProcessScreeningTwo")
	}	

	const handleNext = () => {
		const { keluarga_isWa, keluarga_isTelp, keluarga_wa_no, keluarga_telp_no } = formdomisili
		if ((keluarga_isWa && keluarga_wa_no !== "") ? true : (keluarga_isTelp && keluarga_telp_no !== "")) {
			navigation.navigate("ProcessScreeningThree")
		} else {
			if (keluarga_isWa && keluarga_wa_no === "") {
				Alert.alert("No. Whatsapp Kosong", "Silahkan lengkapi No. WA sebelum melanjutkan.")
			} else if (!keluarga_isWa && keluarga_isTelp && keluarga_telp_no === "") {
				Alert.alert("No. Telepon belum lengkap", "Silahkan lengkapi No. Telepon Pribadi (karena tanpa No. WA) sebelum melanjutkan.")				
			} else if (!keluarga_isWa && !keluarga_isTelp && keluarga_wa_no === "" && keluarga_telp_no === "") {
				Alert.alert("No. Kontak belum lengkap", "Silahkan lengkapi No. WA / No. Telepon Pribadi sebelum melanjutkan.")
			} else {
				Alert.alert("Identitas belum lengkap", "Silahkan lengkapi data identitas sebelum melanjutkan.")
			}
		}
	}

	return (
		<Container style={styles.container}>
			<Header transparent androidStatusBarColor="#fff">
				<Left>
					<Button transparent onPress={handleBack}>
						<Icon style={{ color: "#4F4F4F" }} name="md-arrow-back" />
					</Button>
				</Left>
				<Body>
					<Text style={{ color: "#4F4F4F" }}>Process Screening</Text>
				</Body>
				<Right />
			</Header>

			<ScrollView style={{ marginTop: 8 }} >
				<Content padder>

					<List>
						<Card>
							<CardItem header>
								<Text style={{ color: "#FEAC0E", fontSize: 16 }}>Data Domisili / Alamat Tujuan Tinggal</Text>
							</CardItem>


							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note style={{ marginBottom: 8 }}>Provinsi</Text>
										<Form>
											<Item regular picker>
											<Picker mode="dropdown" iosIcon={<Icon name="arrow-down" />}
													style={{ width: undefined }}
													placeholder="Lorem Ipsum"
													placeholderStyle={{ color: "#bfc6ea" }}
													placeholderIconColor="#007aff"
													selectedValue={formdomisili.prov_id}
													onValueChange={v => handleChangeProvinsi("prov_id", v)}
												>
													<Picker.Item value="" label="-" />
													{provinsi.map(x => (
														<Picker.Item key={x.id} value={x.id} label={ucfirst(x.nama)} />
													))}
												</Picker>
											</Item>
										</Form>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note style={{ marginBottom: 8 }}>Kota/Kabupaten</Text>
										<Form>
											<Item regular picker>
											<Picker mode="dropdown" iosIcon={<Icon name="arrow-down" />}
													style={{ width: undefined }}
													placeholder="Lorem Ipsum"
													placeholderStyle={{ color: "#bfc6ea" }}
													placeholderIconColor="#007aff"
													selectedValue={formdomisili.kab_id}
													onValueChange={v => handleChangeKabupaten("kab_id", v)}
												>
													<Picker.Item value="" label={kabupaten.length > 0 ? "-" : "Pilih Provinsi dahulu"} />
													{kabupaten.map(x => (
														<Picker.Item key={x.id} value={x.id} label={ucfirst(x.nama)} />
													))}
												</Picker>
											</Item>
										</Form>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note style={{ marginBottom: 8 }}>Kecamatan</Text>
										<Form>
											<Item regular picker>
												<Picker mode="dropdown" iosIcon={<Icon name="arrow-down" />}
													style={{ width: undefined }}
													placeholder="Lorem Ipsum"
													placeholderStyle={{ color: "#bfc6ea" }}
													placeholderIconColor="#007aff"
													selectedValue={formdomisili.kec_id}
													onValueChange={v => handleChangeKecamatan("kec_id", v)}
												>
													<Picker.Item value="" label={kecamatan.length > 0 ? "-" : "Pilih Kota/Kabupaten dahulu"} />
													{kecamatan.map(x => (
														<Picker.Item key={x.id} value={x.id} label={ucfirst(x.nama)} />
													))}
												</Picker>
											</Item>
										</Form>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note style={{ marginBottom: 8 }}>Desa/Kelurahan</Text>
										<Form>
											<Item regular picker>
												<Picker mode="dropdown" iosIcon={<Icon name="arrow-down" />}
													style={{ width: undefined }}
													placeholder="Lorem Ipsum"
													placeholderStyle={{ color: "#bfc6ea" }}
													placeholderIconColor="#007aff"
													selectedValue={formdomisili.kel_id}
													onValueChange={v => handleForm("kel_id", v)}
												>
													<Picker.Item value="" label={kelurahan.length > 0 ? "-" : "Pilih Kecamatan dahulu"} />
													{kelurahan.map(x => (
														<Picker.Item key={x.id} value={x.id} label={ucfirst(x.nama)} />
													))}
												</Picker>
											</Item>
										</Form>
									</Body>
								</Left>
							</ListItem>


							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>Dusun</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon active style={{ color: "#FEAC0E" }} name='location-city' type="MaterialIcons" />
											<Input value={formdomisili.dusun} onChangeText={v => handleForm("dusun", v)} placeholder="nama dusun" />
										</Item>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>RT/RW</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon active style={{ color: "#FEAC0E" }} name='location-city' type="MaterialIcons" />
											<Input value={formdomisili.rtrw} onChangeText={v => handleForm("rtrw", v)} placeholder="RT/RW" />
										</Item>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>Alamat</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon active style={{ color: "#FEAC0E" }} name='location-city' type="MaterialIcons" />
											<Input value={formdomisili.alamat} onChangeText={v => handleForm("alamat", v)} placeholder="Alamat Anda" />
										</Item>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>Nama Tujuan</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon active style={{ color: "#FEAC0E" }} name='user' type="SimpleLineIcons" />
											<Input value={formdomisili.keluarga_nama} onChangeText={v => handleForm("keluarga_nama", v)} placeholder="Input Nama" />
										</Item>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>No Telp WA Tujuan</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon style={{ color: "#FEAC0E" }} name='whatsapp' type="FontAwesome" />
											<Input keyboardType="phone-pad" value={formdomisili.keluarga_wa_no} onChangeText={v => handleForm("keluarga_wa_no", v)} onKeyPress={handleEnterNoHp} disabled={!formdomisili.keluarga_isWa} placeholder="Input No. WA" />
										</Item>
									</Body>
								</Left>
								<Right>
									<Text />
									<Badge style={{backgroundColor: "#0677F9", marginBottom: 5 }}>
										<Text style={{fontSize: 12}}>
											{formdomisili.keluarga_isWa ? "Ya" : "Tidak"}
										</Text>
									</Badge>
									<Switch style={{marginBottom: 4}} value={formdomisili.keluarga_isWa} onValueChange={handleCheckWa} />
								</Right>
							</ListItem>
							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>No. Telp Pribadi (Voice Contact) Tujuan</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon style={{ color: "#FEAC0E" }} name='whatsapp' type="FontAwesome" />
											<Input keyboardType="phone-pad" value={formdomisili.keluarga_telp_no} onChangeText={v => handleForm("keluarga_telp_no", v)} disabled={!formdomisili.keluarga_isTelp} placeholder="Input No. Telp Pribadi" />
										</Item>
									</Body>
								</Left>
								<Right>
									<Text />
									<Badge style={{backgroundColor: "#0677F9", marginBottom: 5 }}>
										<Text style={{fontSize: 12}}>
											{formdomisili.keluarga_isTelp ? "Ya" : "Tidak"}
										</Text>
									</Badge>
									<Switch value={formdomisili.keluarga_isTelp} onValueChange={handleCheckTelp} style={{marginBottom: 4}} />
								</Right>
							</ListItem>	

						</Card>
					</List>

					<Button
						block
						style={{ backgroundColor: "#0677F9", marginTop: 16 }}
						onPress={handleNext}>
						<Text>NEXT</Text>
					</Button>

				</Content>
			</ScrollView>
		</Container>
	)
}

export default ProcessScreeningDataLokasiSaatIni
