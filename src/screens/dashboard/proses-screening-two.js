import React from "react"
import { View, Image, StyleSheet, Dimensions, Alert } from "react-native"
import { Body, Button, Card, CardItem, Container, Content, H3, Header, Icon, Left, Right, Text } from "native-base"
import { ScrollView } from "react-native-gesture-handler"
import { useDispatch, useSelector } from "react-redux"
import { RNCamera } from "react-native-camera"
import styles from "./styles"


import { screening_addListKtp, screening_removeListKtp } from "../../stores/screening.action"

const cardImage01 = require("../../../assets/drawer-cover_09.jpg")
const { width, height } = Dimensions.get("window")

const styler = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'black',
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	preview: {
		flex: 1,
		justifyContent: 'flex-end',
		alignItems: 'center',	
		width: width/2,
		height: height - 400
	},
	capture: {
		flex: 0,
		backgroundColor: '#fff',
		borderRadius: 5,
		padding: 15,
		alignSelf: 'center',
		margin: 0,
	},
})

const RenderImage = ({ data }) => {	
	const dispatch = useDispatch()
	const handleRemove = () => dispatch(screening_removeListKtp(data.uri))

	return (
		<Card style={{ width: (width/4)-(width/40) }}>
			<Image source={{ uri: data.uri }} style={{ height: 80, flex: 1 }} />
			<View style={{ flex: 0, bottom: 80, left: 67, flexDirection: 'row', height: 0, justifyContent: 'flex-start' }}>
				<Icon active onPress={handleRemove} style={{ flex: 0, height: 50, color: "white", opacity: 0.7, fontSize: 20 }} name='circle-with-cross' type="Entypo" />
			</View>
		</Card>
	)
}

const NoImage = () => (
	<Image source={cardImage01} style={{ height: 80, width: (width/4), flex: 1 }} />
)

const ProcessScreeningTwo = ({ navigation }) => {
	const { listktp, formkontakcepat } = useSelector(state => state.screening)
	const dispatch = useDispatch()

	let cameraRef

	const takePicture = async () => {
		if (cameraRef) {
			const options = { quality: 0.3 , base64: true }
			await cameraRef.takePictureAsync(options)
			.then(data => {				
				dispatch(screening_addListKtp(data))
			})
			.catch(err => alert(err))
		}
	}
	const handleBack = () => {
		navigation.navigate("ProcessScreeningOne")
	}
	const handleNext = () => {
		if (formkontakcepat.telp_no !== "") {
			const sumImageIdentitas = listktp.filter(x => x.base64)
			if (sumImageIdentitas.length > 0) {
				navigation.navigate("ProcessScreeningDataLokasiSaatIni")
			} else {
				Alert.alert("Peringatan", "Foto identitas harus ada minimal 1 untuk melanjutkan.")
			}
		} else {
			navigation.navigate("ProcessScreeningDataLokasiSaatIni")
		}
	}

	return (
		<Container style={styles.container}>
			<Header transparent androidStatusBarColor="#fff">
				<Left>
					<Button transparent onPress={handleBack}>
						<Icon style={{ color: "#4F4F4F" }} name="md-arrow-back" />
					</Button>
				</Left>
				<Body>
					<Text style={{ color: "#4F4F4F" }}>Process Screening</Text>
				</Body>
				<Right />
			</Header>

			<ScrollView style={{ marginTop: 8 }} >
				<Content padder>

					<Card>
						<CardItem header style={{ width: "100%" }}>
							<H3 style={{ color: "#FEAC0E" }}>Foto Kartu Identitas Warga</H3>							
						</CardItem>
						<CardItem cardBody>
							{/* <Image source={cardImage01} style={{ height: 300, width: null, flex: 1 }} /> */}
							<View style={styler.container}>
								<RNCamera
									ref={ref => cameraRef = ref}
									style={styler.preview}
									type={RNCamera.Constants.Type.back}
									flashMode={RNCamera.Constants.FlashMode.off}
									androidCameraPermissionOptions={{
										title: 'Permission to use camera',
										message: 'We need your permission to use your camera',
										buttonPositive: 'Ok',
										buttonNegative: 'Cancel',
									}}
								/>
								<View style={{ flex: 0, bottom: 50, flexDirection: 'row', height: 0, justifyContent: 'flex-start' }}>
									<Button onPress={takePicture} style={styler.capture}>
										<Icon active style={{ color: "#FEAC0E" }} name='camera' type="Entypo" />
									</Button>
								</View>
							</View>
						</CardItem>
					</Card>
					<View style={{ justifyContent: 'flex-start', flexDirection: 'row', flexWrap: 'wrap', width: '100%' }}>
						{listktp && listktp.filter(x => x.base64).map((data, i) => (
							<View key={i}>
								{data.uri ? <RenderImage data={data} /> : <NoImage />}								
							</View>
						))}
					</View>
					
					<Button
						onPress={handleNext}
						block
						style={{ backgroundColor: "#0677F9", marginTop: 16 }}>
						<Text>NEXT</Text>
					</Button>

				</Content>
			</ScrollView>
		</Container>
	)
}

export default ProcessScreeningTwo
