import React, { useState } from "react"
import { Body, DatePicker, Button, Card, CardItem, Col, Container, Content, Form, Grid, H3, Header, Icon, Input, Item, Left, List, ListItem, Picker, Right, Switch, Text } from "native-base"
import { ScrollView } from "react-native-gesture-handler"
import styles from "./styles"
import moment from "moment"
import { useSelector, useDispatch } from "react-redux"

import { screening_handleFormKesehatan } from "../../stores/screening.action"

const ProcessScreeningFive = ({ navigation }) => {
	const { screening: { formkesehatan } } = useSelector(state => state)
	const dispatch = useDispatch() 

	const handleForm = (name, value) => {		
		dispatch(screening_handleFormKesehatan(name, value))
	}
	const handleDate = (name, value) => {		
		dispatch(screening_handleFormKesehatan(name, moment(value).formkesehatanat("YYYY-MM-DD")))
	}

	const handleNext = () => {
		navigation.navigate("ProcessScreeningSix")
	}

	return (
		<Container style={styles.container}>
			<Header transparent androidStatusBarColor="#fff">
				<Left>
					<Button transparent onPress={() => navigation.navigate("ProcessScreeningThree")}>
						<Icon style={{ color: "#4F4F4F" }} name="md-arrow-back" />
					</Button>
				</Left>
				<Body>
					<Text style={{ color: "#4F4F4F" }}>Process Screening</Text>
				</Body>
				<Right />
			</Header>

			<ScrollView style={{ marginTop: 8 }} >
				<Content padder>

					<List>
						<Card>
							<CardItem header>
								<H3 style={{ color: "#FEAC0E" }}>Status Kesehatan</H3>
							</CardItem>
					
							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Grid style={{ marginBottom: 16 }}>
										<Col style={{ flex: 3 }}>
												<Text note style={{ marginBottom: 8 }}>Demam</Text>
												<Item regular style={{ backgroundColor: "white" }}>
													<Input value={formkesehatan.demam} onChangeText={v => handleForm("demam", v)} disabled placeholder="Demam" />
												</Item>
											</Col>
											<Col style={{ flex: 2 }}>
												<Text note style={{ marginBottom: 8 }}>Suhu</Text>
												<Item regular style={{ backgroundColor: "white", marginLeft: 8 }}>
													<Input value={formkesehatan.suhu} onChangeText={v => handleForm("suhu", v)} placeholder="°C" />
												</Item>
											</Col>
											<Col style={{ flex: 2 }}>
												<Text style={{ marginBottom: 8 }}></Text>
												<Button block onPress={() => handleForm("demam", "ya")} style={{ opacity: (formkesehatan.demam === "ya" ? 1 : 0.4), marginLeft: 8, backgroundColor: "#05C07C" }}><Text>YA</Text></Button>
											</Col>
											<Col style={{ flex: 3 }}>
												<Text style={{ marginBottom: 8 }}></Text>
												<Button block danger onPress={() => handleForm("demam", "tidak")} style={{ opacity: (formkesehatan.demam === "tidak" ? 1 : 0.4), marginLeft: 8, backgroundColor: "#DA132F" }}><Text>TIDAK</Text></Button>
											</Col>
										</Grid>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Grid style={{ marginBottom: 16 }}>
											<Col style={{ flex: 4 }}>
												<Text note style={{ marginBottom: 8 }}>Batuk</Text>
												<Item regular style={{ backgroundColor: "white" }}>
													<Input value={formkesehatan.batuk} onChangeText={v => handleForm("batuk", v)} disabled placeholder="Batuk" />
												</Item>
											</Col>
											<Col style={{ flex: 2 }}>
												<Text style={{ marginBottom: 8 }}></Text>
												<Button block onPress={() => handleForm("batuk", "ya")} style={{ opacity: (formkesehatan.batuk === "ya" ? 1 : 0.4), marginLeft: 8, backgroundColor: "#05C07C" }}><Text>YA</Text></Button>
											</Col>
											<Col style={{ flex: 3 }}>
												<Text style={{ marginBottom: 8 }}></Text>
												<Button block danger onPress={() => handleForm("batuk", "tidak")} style={{ opacity: (formkesehatan.batuk === "tidak" ? 1 : 0.4), marginLeft: 8, backgroundColor: "#DA132F" }}><Text>TIDAK</Text></Button>
											</Col>
										</Grid>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Grid style={{ marginBottom: 16 }}>
											<Col style={{ flex: 4 }}>
												<Text note style={{ marginBottom: 8 }}>Sakit Tenggorokan</Text>
												<Item regular style={{ backgroundColor: "white" }}>
													<Input value={formkesehatan.sakit_tenggorokan} onChangeText={v => handleForm("sakit_tenggorokan", v)} disabled placeholder="Sakit Tenggorokan" />
												</Item>
											</Col>
											<Col style={{ flex: 2 }}>
												<Text style={{ marginBottom: 8 }}></Text>
												<Button block onPress={() => handleForm("sakit_tenggorokan", "ya")} style={{ opacity: (formkesehatan.sakit_tenggorokan === "ya" ? 1 : 0.4), marginLeft: 8, backgroundColor: "#05C07C" }}><Text>YA</Text></Button>
											</Col>
											<Col style={{ flex: 3 }}>
												<Text style={{ marginBottom: 8 }}></Text>
												<Button block danger onPress={() => handleForm("sakit_tenggorokan", "tidak")} style={{ opacity: (formkesehatan.sakit_tenggorokan === "tidak" ? 1 : 0.4), marginLeft: 8, backgroundColor: "#DA132F" }}><Text>TIDAK</Text></Button>
											</Col>
										</Grid>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Grid style={{ marginBottom: 16 }}>
											<Col style={{ flex: 4 }}>
												<Text note style={{ marginBottom: 8 }}>Sakit Kepala</Text>
												<Item regular style={{ backgroundColor: "white" }}>
													<Input value={formkesehatan.sakit_kepala} onChangeText={v => handleForm("sakit_kepala", v)} disabled placeholder="Sakit Kepala" />
												</Item>
											</Col>
											<Col style={{ flex: 2 }}>
												<Text style={{ marginBottom: 8 }}></Text>
												<Button block onPress={() => handleForm("sakit_kepala", "ya")} style={{ opacity: (formkesehatan.sakit_kepala === "ya" ? 1 : 0.4), marginLeft: 8, backgroundColor: "#05C07C" }}><Text>YA</Text></Button>
											</Col>
											<Col style={{ flex: 3 }}>
												<Text style={{ marginBottom: 8 }}></Text>
												<Button block danger onPress={() => handleForm("sakit_kepala", "tidak")} style={{ opacity: (formkesehatan.sakit_kepala === "tidak" ? 1 : 0.4), marginLeft: 8, backgroundColor: "#DA132F" }}><Text>TIDAK</Text></Button>
											</Col>
										</Grid>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Grid style={{ marginBottom: 16 }}>
											<Col style={{ flex: 4 }}>
												<Text note style={{ marginBottom: 8 }}>Lemas</Text>
												<Item regular style={{ backgroundColor: "white" }}>
													<Input value={formkesehatan.lemas} onChangeText={v => handleForm("lemas", v)} placeholder="Lemas" />
												</Item>
											</Col>
											<Col style={{ flex: 2 }}>
												<Text style={{ marginBottom: 8 }}></Text>
												<Button block onPress={() => handleForm("lemas", "ya")} style={{ opacity: (formkesehatan.lemas === "ya" ? 1 : 0.4), marginLeft: 8, backgroundColor: "#05C07C" }}><Text>YA</Text></Button>
											</Col>
											<Col style={{ flex: 3 }}>
												<Text style={{ marginBottom: 8 }}></Text>
												<Button block danger onPress={() => handleForm("lemas", "tidak")} style={{ opacity: (formkesehatan.lemas === "tidak" ? 1 : 0.4), marginLeft: 8, backgroundColor: "#DA132F" }}><Text>TIDAK</Text></Button>
											</Col>
										</Grid>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Grid style={{ marginBottom: 16 }}>
											<Col style={{ flex: 4 }}>
												<Text note style={{ marginBottom: 8 }}>Sesak Napas</Text>
												<Item regular style={{ backgroundColor: "white" }}>
													<Input value={formkesehatan.sesak_napas} onChangeText={v => handleForm("sesak_napas", v)} disabled placeholder="Sesak Napas" />
												</Item>
											</Col>
											<Col style={{ flex: 2 }}>
												<Text style={{ marginBottom: 8 }}></Text>
												<Button block onPress={() => handleForm("sesak_napas", "ya")} style={{ opacity: (formkesehatan.sesak_napas === "ya" ? 1 : 0.4), marginLeft: 8, backgroundColor: "#05C07C" }}><Text>YA</Text></Button>
											</Col>
											<Col style={{ flex: 3 }}>
												<Text style={{ marginBottom: 8 }}></Text>
												<Button block danger onPress={() => handleForm("sesak_napas", "tidak")} style={{ opacity: (formkesehatan.sesak_napas === "tidak" ? 1 : 0.4), marginLeft: 8, backgroundColor: "#DA132F" }}><Text>TIDAK</Text></Button>
											</Col>
										</Grid>
									</Body>
								</Left>
							</ListItem>
							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note style={{ marginBottom: 8 }}>Status Kondisi</Text>
										<Form>
											<Item regular picker>
												<Picker mode="dropdown" iosIcon={<Icon name="arrow-down" />}
													style={{ width: undefined }}
													placeholder="Lorem Ipsum"
													placeholderStyle={{ color: "#bfc6ea" }}
													placeholderIconColor="#007aff"
													selectedValue={formkesehatan.status_kondisi}
													onValueChange={v => handleForm("status_kondisi", v)}
												>
													<Picker.Item label="OTG" value={1} />
													<Picker.Item label="ODP* - Ringan" value={2} />
													<Picker.Item label="ODP* - Berat" value={3} />
												</Picker>
											</Item>
										</Form>
									</Body>
								</Left>
							</ListItem>

							<CardItem header>
								<H3 style={{ color: "#FEAC0E" }}>Rujukan Pasien Ke</H3>
							</CardItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Grid style={{ marginBottom: 16 }}>
											<Col style={{ flex: 4 }}>
												<Col style={{ flex: 2 }}>
													<Text note style={{ marginBottom: 8 }}>Faskes</Text>
													<Item regular style={{ backgroundColor: "white", marginLeft: 0 }}>
														<Input value={formkesehatan.faskes} onChangeText={v => handleForm("faskes", v)} placeholder="Faskes" />
													</Item>
												</Col>
											</Col>
											<Col style={{ flex: 3 }}>
												<Text style={{ marginBottom: 8 }}></Text>
												<Button danger block 
													onPress={() => navigation.navigate("ProcessScreeningSeven")}
													style={{ marginLeft: 8, backgroundColor: "#007aff" }}><Text>INFO FASKES</Text></Button>
											</Col>
										</Grid>
									</Body>
								</Left>
							</ListItem>

							<ListItem>
								<Left>
									<Body style={{ marginLeft: 0 }}>
										<Text note>Hari dan Tanggal</Text>
										<Item regular style={{ marginTop: 8, backgroundColor: "white" }}>
											<Icon active style={{ color: "#FEAC0E" }} name="calendar" type="SimpleLineIcons" />
											<DatePicker
												defaultDate={moment(formkesehatan.tanggal).toDate() || moment().toDate()}
												minimumDate={new Date(1945, 1, 1)}
												maximumDate={new Date(2020, 12, 12)}
												locale={"en"}
												timeZoneOffsetInMinutes={undefined}
												modalTransparent={false}
												animationType={"fade"}
												androidMode={"default"}
												placeHolderText="Select date"
												textStyle={{ color: "black" }}
												placeHolderTextStyle={{ color: "#d3d3d3" }}
												onDateChange={v => handleDate("tanggal", v)}
												disabled={false} />
										</Item>
									</Body>
								</Left>
							</ListItem>

						</Card>
					</List>

					<Button
						block
						style={{ backgroundColor: "#0677F9", marginTop: 16 }}
						onPress={handleNext}>
						<Text>NEXT</Text>
					</Button>

				</Content>
			</ScrollView>
		</Container>
	)
}

export default ProcessScreeningFive
