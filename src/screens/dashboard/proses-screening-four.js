import { Body, H3, DatePicker, Button, Card, Container, Content, CardItem, Header, Icon, Input, Item, Left, List, ListItem, Right, Text } from "native-base";
import React, { Component } from "react";
import { Image } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import styles from "./styles";

const cardImage01 = require("../../../assets/drawer-cover_09.jpg");


class ProcessScreeningFour extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selected2: undefined
    };
  }
  onValueChange2(value: string) {
    this.setState({
      selected2: value
    });
  }

  render() {
    return (
      <Container style={styles.container}>
        <Header transparent androidStatusBarColor="#fff">
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate("ProcessScreeningThree")}>
              <Icon style={{color:"#4F4F4F"}} name="md-arrow-back" />
            </Button>
          </Left>
          <Body>  
            <Text style={{color: "#4F4F4F"}}>Process Screening</Text>
          </Body>
          <Right/>
        </Header>

        <ScrollView style={{marginTop: 8}} >
          <Content padder>

            <Card>
              <CardItem header>
                  <H3 style={{color: "#FEAC0E"}}>Foto 3</H3>
                </CardItem>
              <CardItem cardBody style={{position: "relative"}}>
                  <Image source={cardImage01} style={{height: 300, width: null, flex: 1}} />
              </CardItem>
            </Card>

            <Button 
              onPress={() => this.props.navigation.navigate("ProcessScreeningFive")}
              block 
              style={{backgroundColor: "#0677F9", marginTop: 16}}>
                <Text>NEXT</Text>
            </Button>

          </Content>
        </ScrollView>
      </Container>
    );
  }
}

export default ProcessScreeningFour;
