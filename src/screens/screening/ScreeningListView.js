import React, {useState, useEffect} from "react"
import {useSelector, useDispatch} from "react-redux"
import {View, Modal, TouchableOpacity, Image} from "react-native"
import {
  Badge,
  DatePicker,
  Thumbnail,
  Form,
  Picker,
  Body,
  Button,
  Card,
  Container,
  Content,
  Footer,
  FooterTab,
  H3,
  Header,
  Icon,
  Input,
  Item,
  Left,
  List,
  ListItem,
  Right,
  Text,
  Title,
} from "native-base"
import {ScrollView} from "react-native-gesture-handler"
import styles from "./styles"
import {get} from "../../helpers/fetch"
import {screening_history} from "../../stores/screening.action"

const cardImage07 = require("../../../assets/drawer-cover_07.jpg")

const ScreeningListView = ({navigation}) => {
  const [tgl_mulai, setTgl_mulai] = useState("")
  const [tgl_akhir, setTgl_akhir] = useState("")
  const {historyscreening} = useSelector((state) => state.screening)
  console.log(historyscreening)
  const dispatch = useDispatch()

  const [visible, setVisible] = useState(false)
  const [search, setSearch] = useState({
    status_kesehatan: "",
  })
  const [filterValue, setValue] = useState("key0")

  const [filterNama, setFilterNama] = useState("")

  const handleFilterNama = (filterNama) => {
    get(`screening`, {
      nama: {filterNama},
    }).then((res) => {
      dispatch(screening_history(res.data))
      console.log(filterNama)
      console.log("resf", res)
    })
    setVisible(false)
  }

  const handleFilter = () => {
    get(`screening`, {
      tgl_mulai: {tgl_mulai},
      tgl_akhir: {tgl_akhir},
      id_status_covid: {filterValue},
    }).then((res) => {
      dispatch(screening_history(res.data))
    })
    setVisible(false)
  }

  const handleBack = () => {
    navigation.navigate("Dashboard")
  }

  const handlePicker = (filterValue) => {
    setValue(filterValue)
  }

  useEffect(() => {
    get(`screening`).then((res) => {
      dispatch(screening_history(res.data))
      console.log("res", res)
    })
  }, [])

  return (
    <Container style={styles.container}>
      <Header transparent androidStatusBarColor="#fff">
        <Left>
          <Image
            style={{height: 30}}
            source={require("../../assets/img/logo-covid-header.png")}
          />
        </Left>
        <Body style={{marginLeft: 55}}>
          <Title style={{color: "#4F4F4F"}}>Screening</Title>
        </Body>
      </Header>

      <ScrollView style={{marginTop: 5}}>
        <Content padder>
          <Modal
            animationType="slide"
            transparent={false}
            visible={visible}
            onRequestClose={handleFilter}>
            <View style={{marginTop: 10, paddingHorizontal: 16}}>
              <H3
                style={{
                  color: "#FEAC0E",
                  fontWeight: "normal",
                  marginBottom: 16,
                }}>
                Filter Screening
              </H3>
              <List>
                <Card>
                  <ListItem>
                    <Left>
                      <Body>
                        <Text note>Pilih tanggal mulai</Text>
                        <Item
                          regular
                          style={{marginTop: 8, backgroundColor: "white"}}>
                          <Icon
                            active
                            style={{color: "#FEAC0E"}}
                            name="calendar"
                            type="SimpleLineIcons"
                          />
                          <DatePicker
                            defaultDate={new Date(2020, 1, 1)}
                            minimumDate={new Date(1945, 1, 1)}
                            maximumDate={new Date(2020, 12, 12)}
                            locale={"en"}
                            timeZoneOffsetInMinutes={undefined}
                            modalTransparent={false}
                            animationType={"fade"}
                            androidMode={"default"}
                            placeHolderText="Select date"
                            textStyle={{color: "black"}}
                            placeHolderTextStyle={{color: "#d3d3d3"}}
                            onDateChange={setTgl_mulai}
                            disabled={false}
                          />
                        </Item>
                      </Body>
                    </Left>
                  </ListItem>

                  <ListItem>
                    <Left>
                      <Body>
                        <Text note>Pilih tanggal akhir</Text>
                        <Item
                          regular
                          style={{marginTop: 8, backgroundColor: "white"}}>
                          <Icon
                            active
                            style={{color: "#FEAC0E"}}
                            name="calendar"
                            type="SimpleLineIcons"
                          />
                          <DatePicker
                            defaultDate={new Date(2020, 1, 1)}
                            minimumDate={new Date(1945, 1, 1)}
                            maximumDate={new Date(2020, 12, 12)}
                            locale={"en"}
                            timeZoneOffsetInMinutes={undefined}
                            modalTransparent={false}
                            animationType={"fade"}
                            androidMode={"default"}
                            placeHolderText="Select date"
                            textStyle={{color: "black"}}
                            placeHolderTextStyle={{color: "#d3d3d3"}}
                            onDateChange={setTgl_akhir}
                            disabled={false}
                          />
                        </Item>
                      </Body>
                    </Left>
                  </ListItem>

                  <ListItem>
                    <Left>
                      <Body>
                        <Text note>Pilih Status Kondisi Kesehatan</Text>
                        <Form>
                          <Item picker>
                            <Picker
                              mode="dropdown"
                              iosIcon={<Icon name="arrow-down" />}
                              style={{width: undefined}}
                              placeholder="Select your SIM"
                              placeholderStyle={{color: "#bfc6ea"}}
                              placeholderIconColor="#007aff"
                              selectedValue={filterValue}
                              onValueChange={(filterValue) =>
                                handlePicker(filterValue)
                              }>
                              <Picker.Item label="OTG" value="key0" />
                              <Picker.Item label="ODP* - Ringan" value="key1" />
                              <Picker.Item label="ODP* - Berat" value="key2" />
                            </Picker>
                          </Item>
                        </Form>
                      </Body>
                    </Left>
                  </ListItem>
                </Card>
              </List>

              <Button
                onPress={handleFilter}
                style={{backgroundColor: "#2D9CDB", marginTop: 8}}
                block>
                <Text>FILTER</Text>
              </Button>
            </View>
          </Modal>

          <Card
            style={{
              backgroundColor: "#0988FD",
              marginBottom: 16,
              borderRadius: 8,
              padding: -30,
            }}>
            <List>
              <ListItem style={{marginLeft: 0}} noBorder>
                <Left>
                  <Item
                    rounded
                    style={{
                      backgroundColor: "white",
                      marginLeft: 12,
                      marginRight: -30,
                    }}>
                    <Input
                      placeholder="Cari berdasarkan nama"
                      onSubmitEditing={handleFilterNama}
                      value={filterNama}
                      onChange={(filterNama) => setFilterNama(filterNama)}
                    />
                    <Icon
                      style={{color: "#4F4F4F"}}
                      name="magnifier"
                      type="SimpleLineIcons"
                    />
                  </Item>
                </Left>
                <Right>
                  <TouchableOpacity onPress={() => setVisible(true)}>
                    <Icon
                      style={{fontSize: 30, color: "#fff"}}
                      name="filter"
                      type="Feather"
                    />
                    <Text
                      style={{
                        fontSize: 12,
                        color: "#fff",
                        justifyContent: "center",
                      }}>
                      Filter
                    </Text>
                  </TouchableOpacity>
                </Right>
              </ListItem>
            </List>
          </Card>

          <List>
            {!!historyscreening &&
              historyscreening.map((item) => {
                return (
                  <Card>
                    <ListItem
                      onPress={() =>
                        navigation.navigate("ScreeningProcessScreeningOne", {
                          item,
                        })
                      }
                      thumbnail
                      style={{marginLeft: 4}}>
                      <Left>
                        <Thumbnail square source={cardImage07} />
                      </Left>

                      <Body>
                        <View
                          style={{
                            justifyContent: "space-between",
                            flexDirection: "row",
                          }}>
                          <Text>{item.nama}</Text>
                          <Badge
                            style={{
                              backgroundColor: "#36d167",
                              marginRight: 8,
                            }}>
                            <Text style={{fontSize: 10}}>OTG</Text>
                          </Badge>
                        </View>
                        <Text
                          style={{
                            color: "#F2994A",
                            marginTop: 0,
                            borderBottomColor: "grey",
                            borderBottomWidth: 1,
                            paddingBottom: 8,
                          }}
                          note
                          numberOfLines={1}>
                          {item.no_identitas}
                        </Text>

                        <Text
                          style={{
                            fontSize: 12,
                            fontWeight: "normal",
                            color: "#4F4F4F",
                            marginTop: 8,
                          }}>
                          Kontak
                        </Text>
                        <Text style={{fontWeight: "bold", color: "#4F4F4F"}}>
                          0822 7458 6011
                        </Text>
                      </Body>
                    </ListItem>
                  </Card>
                )
              })}
          </List>
        </Content>
      </ScrollView>

      <Footer>
        <FooterTab style={{backgroundColor: "white"}}>
          <Button
            vertical
            style={{backgroundColor: "white"}}
            onPress={() => navigation.navigate("Dashboard")}>
            <Icon style={{color: "grey"}} type="AntDesign" name="home" />
            <Text style={{color: "grey", fontSize: 9}}>Home</Text>
          </Button>
          <Button vertical style={{backgroundColor: "white"}}>
            <Icon
              style={{color: "grey"}}
              type="AntDesign"
              name="notification"
            />
            <Text style={{fontSize: 9}}>Reminder</Text>
          </Button>
          <Button vertical style={{backgroundColor: "white"}}>
            <Icon
              style={{color: "#FEAC0E"}}
              type="AntDesign"
              name="solution1"
            />
            <Text style={{fontSize: 9, color: "#FEAC0E"}}>Screening</Text>
          </Button>
          <Button
            vertical
            style={{backgroundColor: "white"}}
            onPress={() => navigation.navigate("MyProfile")}>
            <Icon style={{color: "grey"}} type="AntDesign" name="user" />
            <Text style={{fontSize: 9}}>Profile</Text>
          </Button>
        </FooterTab>
      </Footer>
    </Container>
  )
}

export default ScreeningListView
