import {
    Badge,
    Body,
    Button,
    Card,
    CardItem,
    Col,
    Container,
    Content,
    DatePicker,
    Form,
    Grid,
    H3,
    Header,
    Icon,
    Input,
    Item,
    Left,
    List,
    ListItem,
    Picker,
    Text,
    Title,
  } from "native-base"
  import React from "react"
  
  const ScreenProsesIdentitas = (props) => {
    if (props.hide) {
        return (
          null
        )
      }
    return (
      <Card>
        <CardItem header>
          <Text style={{color: "#FEAC0E"}}>Data Domisili / Alamat Tujuan Tinggal</Text>
        </CardItem>
        <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Provinsi</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location"
                type="Entypo"
              />
              <Input placeholder={props.provinsi} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Kota/Kabupaten</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location"
                type="Entypo"
              />
              <Input placeholder={props.kota} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Kecamatan</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location"
                type="Entypo"
              />
              <Input placeholder={props.kecamatan} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Desa/Kelurahan</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location"
                type="Entypo"
              />
              <Input placeholder={props.desa} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Dusun</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location-city"
                type="MaterialIcons"
              />
              <Input placeholder={props.dusun} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>RT/RW</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location-city"
                type="MaterialIcons"
              />
              <Input placeholder={props.rt} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Alamat</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location-city"
                type="MaterialIcons"
              />
              <Input placeholder="Alamat" disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>
      </Card>
    )
  }
  
  export default ScreenProsesIdentitas
  