import {
    Card,
    CardItem,
    Text,
  } from "native-base"
  import React from "react"
  
  const ScreenProsesGallery = (props) => {
    if (props.hide) {
      return (
        null
      )
    }
  
    return (
      <Card>
        <CardItem header>
          <Text style={{color: "#FEAC0E"}}>Gallery Foto Identitas</Text>
        </CardItem>
  
        <CardItem header>
          <Text style={{color: "#FEAC0E"}}>Gallery Foto Yang di Screening</Text>
        </CardItem>
      </Card>
    )
  }
  
  export default ScreenProsesGallery
  