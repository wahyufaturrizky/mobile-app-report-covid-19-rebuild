import { Badge, Body, Button, Card, Container, Content, Footer, FooterTab, Form, H3, Header, Icon, Input, Item, Left, List, ListItem, Picker, Right, Text, Thumbnail, Title } from "native-base";
import React, { Component } from "react";
import { Modal, TouchableOpacity, View } from 'react-native';
import { ScrollView } from "react-native-gesture-handler";
import styles from "./styles";

const cardImage07 = require("../../../assets/drawer-cover_07.jpg");


class ScreeningProcessScreeningThree extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selected2: undefined,
      modalVisible: false,
    };
  }
  onValueChange2(value: string) {
    this.setState({
      selected2: value
    });
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    return (
      <Container style={styles.container}>
        <Header transparent androidStatusBarColor="#fff">
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate("ScreeningProcessScreeningTwo")}>
              <Icon style={{color:"#4F4F4F"}} name="md-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={{color: "#4F4F4F"}}>List Spare Part</Title>
          </Body>
        </Header>

        <ScrollView style={{marginTop: 8}} >
          <Content padder>

            <H3 style={{color: "#FEAC0E", fontWeight: "normal", marginBottom: 16}}>List Screening</H3>

              <List>
                <Card>
                  <ListItem 
                    onPress={() => this.props.navigation.navigate("DetailSparePart")}
                    style={{marginLeft: 4}}>
  
                    <Body>
                      <View style={{justifyContent: "space-between", flexDirection: "row"}}>
                        <Text>Wahyu Fatur Rizki</Text>
                        <Badge style={{backgroundColor: "#04B974", marginRight: 8}}>
                          <Text>07 April 2020</Text>
                        </Badge>
                      </View>
                      <Text style={{color: "#F2994A", marginTop: 0}} note numberOfLines={1}>123456789</Text>
                    </Body>
                  </ListItem>
                </Card>
                <Card>
                  <ListItem 
                    onPress={() => this.props.navigation.navigate("DetailSparePart")}
                    style={{marginLeft: 4}}>
  
                    <Body>
                      <View style={{justifyContent: "space-between", flexDirection: "row"}}>
                        <Text>Wahyu Fatur Rizki</Text>
                        <Badge style={{backgroundColor: "#04B974", marginRight: 8}}>
                          <Text>07 April 2020</Text>
                        </Badge>
                      </View>
                      <Text style={{color: "#F2994A", marginTop: 0}} note numberOfLines={1}>123456789</Text>
                    </Body>
                  </ListItem>
                </Card>
                <Card>
                  <ListItem 
                    onPress={() => this.props.navigation.navigate("DetailSparePart")}
                    style={{marginLeft: 4}}>
  
                    <Body>
                      <View style={{justifyContent: "space-between", flexDirection: "row"}}>
                        <Text>Wahyu Fatur Rizki</Text>
                        <Badge style={{backgroundColor: "#04B974", marginRight: 8}}>
                          <Text>07 April 2020</Text>
                        </Badge>
                      </View>
                      <Text style={{color: "#F2994A", marginTop: 0}} note numberOfLines={1}>123456789</Text>
                    </Body>
                  </ListItem>
                </Card>
              </List>

          </Content>
        </ScrollView>
      </Container>
    );
  }
}

export default ScreeningProcessScreeningThree;
