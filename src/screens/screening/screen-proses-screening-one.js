import { Badge, Body, Button, Card, Container, Content, H3, Header, Icon, Left, List, ListItem, Text, Title } from "native-base";
import React from "react";
import { View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import styles from "./styles";
import { useNavigation } from '@react-navigation/native';

const ScreeningProcessScreeningOne = ({navigation, route}) => {
  const item = route.params.item
  const { navigate } = useNavigation();
    return (
      <Container style={styles.container}>
        <Header transparent androidStatusBarColor="#fff">
          <Left>
            <Button transparent onPress={()=> navigate("ScreeningListView")}>
              <Icon style={{color:"#4F4F4F"}} name="md-arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={{color: "#4F4F4F"}}>Process Screening</Title>
          </Body>
        </Header>

        <ScrollView style={{marginTop: 8}}>
          <Content padder>

            <List>
              <Card>
                <ListItem onPress={()=> navigation.navigate("ScreeningProcessScreeningTwo", {item})}
                  style={{marginLeft: 4}}>

                  <Body>
                    <View style={{justifyContent: "space-between", flexDirection: "row"}}>
                      <Text>{item.nama}</Text>
                      <Badge style={{backgroundColor: "#04B974", marginRight: 8}}>
                        <Text>{item.date_entry}</Text>
                      </Badge>
                    </View>
                    <Text style={{color: "#F2994A", marginTop: 0}} note numberOfLines={1}>{item.id}</Text>
                  </Body>
                </ListItem>
              </Card>
            </List>
          </Content>
        </ScrollView>
      </Container>
    );
  
}

export default ScreeningProcessScreeningOne;
