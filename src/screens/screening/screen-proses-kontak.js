import {
  Body,
  Card,
  CardItem,
  Icon,
  Input,
  Item,
  Left,
  ListItem,
  Text,
} from "native-base"
import React from "react"

const ScreenProsesKontak = (props) => {
  if (props.hide) {
    return null
  }

  return (
    <Card>
      <CardItem header>
        <Text style={{color: "#FEAC0E"}}>Kontak</Text>
      </CardItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>No. Whatsapp</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="smartphone"
                type="MaterialIcons"
              />
              <Input placeholder="No Whatsapp" disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>No. Tlp/Voice Contact</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon style={{color: "#FEAC0E"}} name="call" type="MaterialIcons" />
              <Input placeholder="No. Telp" disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>
    </Card>
  )
}

export default ScreenProsesKontak
