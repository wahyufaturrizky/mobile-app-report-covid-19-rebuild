import {
  Body,
  Card,
  CardItem,
  List,
  Left,
  ListItem,
  Text,
  Right,
} from "native-base"
import React from "react"

const ScreenProsesRiwayat = (props) => {
  if (props.hide) {
    return null
  }

  return (
    <Card style={{padding: 10}}>
      <CardItem header>
        <Text style={{color: "#FEAC0E"}}>
          Riwayat Perjalanan / Persinggahan
        </Text>
      </CardItem>
      <List>
        <Card>
          <ListItem>
            <Left>
              <Body>
                <Text
                  style={{fontWeight: "bold", color: "#4F4F4F", fontSize: 10}}>
                  PERJALANAN KE 1
                </Text>
                <Text style={{color: "#FEAC0E", fontSize: 10}}>
                  TRV_123456789
                </Text>
              </Body>
            </Left>
            <Right style={{direction: "rtl", marginStart : -300}}>
                <Text
                  style={{fontWeight: "bold", color: "#4F4F4F", fontSize: 10}}>
                  Tanggal
                </Text>
                <Text style={{color: "#0586FE", fontSize: 10}}>
                  Senin, 10 April 2020
                </Text>
            </Right>
          </ListItem>

          <ListItem>
            <Left>
              <Body>
                <Text
                  style={{
                    fontSize: 12,
                    fontWeight: "normal",
                    color: "#4F4F4F",
                  }}>
                  Rute Perjalanan
                </Text>
                <Text style={{fontWeight: "bold", color: "#4F4F4F"}}>
                  Surabaya - Malang
                </Text>
              </Body>
            </Left>
            <Right>
              <Text
                style={{fontSize: 12, fontWeight: "normal", color: "#4F4F4F"}}>
                Kendaraan
              </Text>
              <Text style={{fontWeight: "bold", color: "#4F4F4F"}}>
                Pribadi
              </Text>
            </Right>
          </ListItem>
        </Card>
        
      </List>
    </Card>
  )
}

export default ScreenProsesRiwayat
