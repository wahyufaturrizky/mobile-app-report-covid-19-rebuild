import {
  Badge,
  Body,
  Button,
  Card,
  CardItem,
  Col,
  Container,
  Content,
  DatePicker,
  Form,
  Grid,
  H3,
  Header,
  Icon,
  Input,
  Item,
  Left,
  List,
  ListItem,
  Picker,
  Text,
  Title,
} from "native-base"
import React, {useState} from "react"
import {View} from "react-native"
import {ScrollView} from "react-native-gesture-handler"
import styles from "./styles"
import ScreenProsesIdentitas from "./screen-proses-identitas"
import ScreenProsesDomisili from "./screen-proses-domisili"
import ScreenProsesGallery from "./screen-proses-gallery"
import ScreenProsesKontak from "./screen-proses-kontak"
import ScreenProsesKesehatan from "./screen-proses-kesehatan"
import ScreenProsesRujukan from "./screen-proses-rujukan"
import ScreenProsesInfoLain from "./screen-proses-info-lain"
import ScreenProsesRiwayat from "./screen-proses-riwayat"

const ScreeningProcessScreeningTwo = ({navigation, route}) => {
  const item = route.params.item
  const [value, setValue] = useState("key0")

  const handlePicker = (value) => {
    setValue(value)
  }

  return (
    <Container style={styles.container}>
      <Header transparent androidStatusBarColor="#fff">
        <Left>
          <Button
            transparent
            onPress={() => navigation.navigate("ScreeningProcessScreeningOne")}>
            <Icon style={{color: "#4F4F4F"}} name="md-arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title style={{color: "#4F4F4F"}}>Process Screening</Title>
        </Body>
      </Header>

      <ScrollView style={{marginTop: 8}}>
        <Content padder>
          <List style={{marginBottom: 16}}>
            <Card>
              <ListItem
                onPress={() =>
                  navigation.navigate("ScreeningProcessScreeningTwo")
                }
                style={{marginLeft: 4}}>
                <Body>
                  <View
                    style={{
                      justifyContent: "space-between",
                      flexDirection: "row",
                    }}>
                    <Text>{item.nama}</Text>
                    <Badge style={{backgroundColor: "#04B974", marginRight: 8}}>
                      <Text>{item.date_entry}</Text>
                    </Badge>
                  </View>
                  <Text
                    style={{color: "#F2994A", marginTop: 0}}
                    note
                    numberOfLines={1}>
                    {item.id}
                  </Text>
                </Body>
              </ListItem>
            </Card>
          </List>

          <Grid style={{marginBottom: 16}}>
            <Col style={{flex: 4}}>
              <Form>
                <Item regular picker>
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="arrow-down" />}
                    style={{width: undefined, height: 40}}
                    placeholderStyle={{color: "#bfc6ea"}}
                    placeholderIconColor="#007aff"
                    selectedValue={value}
                    onValueChange={(value) => handlePicker(value)}>
                    <Picker.Item label="Identitas" value="key0" />
                    <Picker.Item label="Domisili" value="key1" />
                    <Picker.Item label="Gallery" value="key2" />
                    <Picker.Item label="Kontak" value="key3" />
                    <Picker.Item label="Kesehatan" value="key4" />
                    <Picker.Item label="Rujukan" value="key5" />
                    <Picker.Item label="Informasi Lain" value="key6" />
                    <Picker.Item label="Riwayat Perjalanan" value="key7" />
                  </Picker>
                </Item>
              </Form>
            </Col>
            <Col style={{flex: 2}}>
              <Button
                block
                style={{backgroundColor: "#0586FE", marginLeft: 8, height: 40}}>
                <Text>CARI</Text>
              </Button>
            </Col>
          </Grid>

          <List>
            <ScreenProsesIdentitas
              hide={value == "key0" ? false : true}
              id={item.no_identitas}
              nama={item.nama}
              jkelamin={item.jenis_kelamin}
              negara={item.negara}
              provinsi={item.provinsi}
              kota={item.kota_kabupaten}
              kecamatan={item.kecamatan}
              desa={item.desa_kelurahan}
              dusun={item.dusun}
              rt={item.rt}
              rw={item.rw}
            />
            <ScreenProsesDomisili hide={value == "key1" ? false : true} 
              provinsi={item.provinsi}
              kota={item.kota_kabupaten}
              kecamatan={item.kecamatan}
              desa={item.desa_kelurahan}
              dusun={item.dusun}
              rt={item.rt}
              rw={item.rw}
            />
            <ScreenProsesGallery hide={value == "key2" ? false : true} />
            <ScreenProsesKontak hide={value == "key3" ? false : true} />
            <ScreenProsesKesehatan hide={value == "key4" ? false : true}
            status={item.status_covid}
             />
            <ScreenProsesRujukan hide={value == "key5" ? false : true} />
            <ScreenProsesInfoLain hide={value == "key6" ? false : true} />
            <ScreenProsesRiwayat hide={value == "key7" ? false : true} />
          </List>
        </Content>
      </ScrollView>
    </Container>
  )
}

export default ScreeningProcessScreeningTwo
