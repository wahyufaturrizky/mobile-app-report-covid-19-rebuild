import {
  Body,
  Card,
  CardItem,
  Icon,
  Input,
  Item,
  Left,
  ListItem,
  Text,
} from "native-base"
import React from "react"

const ScreenProsesIdentitas = (props) => {
  if (props.hide) {
    return (
      null
    )
  }

  return (
    <Card>
      <CardItem header>
        <Text style={{color: "#FEAC0E"}}>Data Identitas</Text>
      </CardItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>No. KTP</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="idcard"
                type="AntDesign"
              />
              <Input placeholder={props.id} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Nama</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon style={{color: "#FEAC0E"}} name="user" type="AntDesign" />
              <Input placeholder={props.nama} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Tempat, Tanggal Lahir</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location"
                type="Entypo"
              />
              <Input placeholder="Tempat, Tanggal Lahir" disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Jenis Kelamin</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="wc"
                type="MaterialIcons"
              />
              <Input placeholder={props.jkelamin} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Negara</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location"
                type="Entypo"
              />
              <Input placeholder={props.negara} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Provinsi</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location"
                type="Entypo"
              />
              <Input placeholder={props.provinsi} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Kota/Kabupaten</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location"
                type="Entypo"
              />
              <Input placeholder={props.kota} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Kecamatan</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location"
                type="Entypo"
              />
              <Input placeholder={props.kecamatan} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Desa/Kelurahan</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location"
                type="Entypo"
              />
              <Input placeholder={props.desa} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Dusun</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location-city"
                type="MaterialIcons"
              />
              <Input placeholder={props.dusun} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>RT/RW</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location-city"
                type="MaterialIcons"
              />
              <Input placeholder={props.rt} disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>

      <ListItem>
        <Left>
          <Body style={{marginLeft: 0}}>
            <Text note>Alamat</Text>
            <Item regular style={{marginTop: 8, backgroundColor: "white"}}>
              <Icon
                active
                style={{color: "#FEAC0E"}}
                name="location-city"
                type="MaterialIcons"
              />
              <Input placeholder="Alamat" disabled={true} />
            </Item>
          </Body>
        </Left>
      </ListItem>
    </Card>
  )
}

export default ScreenProsesIdentitas
