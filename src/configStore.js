import AsyncStorage from "@react-native-community/async-storage"
import { createStore, combineReducers, applyMiddleware } from "redux"
import thunk from "redux-thunk"
import { persistStore, persistReducer } from "redux-persist"


import root from "./stores/root.store"
import screening from "./stores/screening.store"


const allStore = combineReducers({
	root,
	screening,
})

// Middleware: Redux Persist Config
const persistConfig = {
    key: "root",
    storage: AsyncStorage,
    // Whitelist (Save Specific Reducers)
    // whitelist: [
    //     'authReducer',
    // ],
    // Blacklist (Don't Save Specific Reducers)
    // blacklist: [
    //     'counterReducer',
    // ],
}

// Middleware: Redux Persist Persisted Reducer
const persistedStore = persistReducer(persistConfig, allStore)
// Redux: Store
const store = createStore(
    persistedStore,
    applyMiddleware(
        thunk
    ),
)

let persistor = persistStore(store)

export { store, persistor }