import { baseApi } from "./url"
import { Alert } from "react-native"
import AsyncStorage from "@react-native-community/async-storage"

export const get = async (requestUrl, params) => {
	const parameter = { ...params }
	const base = requestUrl.indexOf("http") !== -1 ? "" : baseApi + "/"
	const esc = encodeURIComponent
	const objectEntries = Object.entries(parameter).filter(([k, v]) => v !== "" && v !== null)
	console.log('objectEntries', objectEntries)
	const queryParams = objectEntries.length > 0 ? "?" + objectEntries.map(([k, v]) => esc(k) + "=" + esc(v)).join("&") : ""
	console.log('queryParams', queryParams)
	return await fetch(base + requestUrl + queryParams, {
		method: "GET",
		mode: "cors",		
		headers: {
			"Accept": "application/json",
			"Content-Type": "text/plain"
		}
	}).then(raw => raw.json())
}

export const post = async (requestUrl, params) => {
	const parameter = { ...params }
	const base = requestUrl.indexOf("http") !== -1 ? "" : baseApi + "/"
	return await fetch(base + requestUrl, {
		method: "POST",
		mode: "cors",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json"
		},
		body: JSON.stringify(parameter)
	}).then(raw => raw.json())
}

export const postFile = async (requestUrl, params) => {
	const parameter = params
	let data = new FormData()
	Object.entries(parameter).map(([key, val]) => data.append(key, val))
	const base = requestUrl.indexOf("http") !== -1 ? "" : baseApi + "/"
	return await fetch(base + requestUrl, {
		method: "POST",
		mode: "cors",
		body: data
	}).then(raw => raw.json())
}

export const put = async (requestUrl, params) => {
	const parameter = { ...params }
	const base = requestUrl.indexOf("http") !== -1 ? "" : baseApi + "/"
	return await fetch(base + requestUrl, {
		method: "PUT",
		mode: "cors",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify(parameter)
	}).then(raw => raw.json())
}

export const del = async (requestUrl, params) => {
	const parameter = { ...params }
	const base = requestUrl.indexOf("http") !== -1 ? "" : baseApi + "/"
	const esc = encodeURIComponent
	const queryParams =
		"?" +
		Object.keys(parameter)
			.map(k => esc(k) + "=" + esc(parameter[k]))
			.join("&")
	return await fetch(base + requestUrl + queryParams, {
		method: "DELETE",
		mode: "cors",
		headers: {
			Accept: "application/json",
			"Content-Type": "text/plain"
		}
	}).then(raw => raw.json())
}