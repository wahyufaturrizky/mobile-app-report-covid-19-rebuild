export default function ucfirst(x) {
    x = x.toLowerCase()
    return x.charAt(0).toUpperCase() + x.slice(1)
}