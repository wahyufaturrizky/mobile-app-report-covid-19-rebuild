const initialState = {
    screeningId: null,
    formktp: {
        isSearchKtp: false,
        status_peserta: "transit",
        ktp_no: "",
        ktp_nama: "",
        jenis_kelamin: "",
        tempat_lahir: "",
        tgl_lahir: "",
        negara_id: 74,
        prov_id: 0,
        kab_id: 0,
        kec_id: 0,
        kel_id: 0,
        dusun: "",
        rtrw: "",
        isWa: true,
        wa_no: "",
        isTelp: true,
        telp_no: "",
    },
    formkontakcepat: {
        isWa: true,
        wa_no: "",
        isTelp: true,
        telp_no: "",
    },
    formnonktp: {
        status_peserta: "transit",
        jenis_identitas: "sim",
        jenis_kelamin: "",
        ktp_no: "",
        ktp_nama: "",
        tempat_lahir: "",
        tgl_lahir: "",
        negara_id: 74,
        prov_id: 0,
        kab_id: 0,
        kec_id: 0,
        kel_id: 0,
        dusun: "",
        rtrw: "",
        isWa: true,
        wa_no: "",
        isTelp: true,
        telp_no: "",
    },
    formdomisili: {
        prov_id: 0,
        kab_id: 0,
        kec_id: 0,
        kel_id: 0,
        dusun: "",
        rtrw: "",
        alamat: "",
        keluarga_nama: "",
        keluarga_isWa: true,
        keluarga_wa_no: "",
        keluarga_isTelp: true,
        keluarga_telp_no: "",
    },
    formkesehatan: {
        status_kesehatan: "sehat",
        demam: "tidak",
        suhu: "",
        batuk: "tidak",
        sakit_tenggorokan: "tidak",
        sakit_kepala: "tidak",
        lemas: "tidak",
        sesak_napas: "tidak",
        status_kondisi: 1,
        faskes: "",
        tanggal: "",
    },
    formlain: {
        tetangga_covid: "tidak",
        catatan: ""
    },
    formriwayat: {
        riwayat_no: "",
        tanggal: "",
        rute: "",
        durasi: "",
        jenis_transportasi: "umum",
    },
    riwayat: [],
    listktp: [{ sample: true }],
    listscreening: [{ sample: true }],
    historyscreening: []
}

const screening = (state = initialState, action) => {
    const { type, payload } = action
    switch (type) {
        case "SET_SCREENING_ID":
            return {
                ...state,
                screeningId: payload
            }
        case "SET_FORM_KTP":
            return {
                ...state,
                formktp: { ...state.formktp, ...payload }
            }
        case "HANDLE_FORM_KTP":
            return {
                ...state,
                formktp: { ...state.formktp, [payload.name]: payload.value }
            }
        case "HANDLE_FORM_NON_KTP":
            return {
                ...state,
                formnonktp: { ...state.formnonktp, [payload.name]: payload.value }
            }
        case "HANDLE_FORM_KONTAK_CEPAT":
            return {
                ...state,
                formkontakcepat: { ...state.formkontakcepat, [payload.name]: payload.value }
            }
        case "HANDLE_FORM_DOMISILI":
            return {
                ...state,
                formdomisili: { ...state.formdomisili, [payload.name]: payload.value }
            }
        case "HANDLE_FORM_KESEHATAN":
            return {
                ...state,
                formkesehatan: { ...state.formkesehatan, [payload.name]: payload.value, status_kondisi: payload.status_kondisi, tanggal: payload.tanggal }
            }
        case "HANDLE_FORM_LAIN":
            return {
                ...state,
                formlain: { ...state.formlain, [payload.name]: payload.value }
            }
        case "HANDLE_FORM_RIWAYAT":
            return {
                ...state,
                formriwayat: { ...state.formriwayat, [payload.name]: payload.value }
            }
        case "SET_RIWAYAT":
            const newPayload = payload
            return {
                ...state,
                riwayat: [...state.riwayat, newPayload],
                formriwayat: {
                    riwayat_no: "",
                    tanggal: "",
                    rute: "",
                    durasi: "",
                    jenis_transportasi: "kendaraan umum",
                },
            }
        case "ADD_LISTKTP":
            return {
                ...state,
                listktp: [...state.listktp, payload]
            }
        case "REMOVE_LISTKTP":
            return {
                ...state,
                listktp: state.listktp.filter(x => x.uri !== payload)
            }
        case "ADD_LISTSCREENING":
            return {
                ...state,
                listscreening: [...state.listscreening, payload]
            }
        case "REMOVE_LISTSCREENING":
            return {
                ...state,
                listscreening: state.listscreening.filter(x => x.uri !== payload)
            }
        case "HISTORY_LISTSCREENING":
            return {
                ...state,
                historyscreening: payload
            }

        case "RESET":
            return {
                ...state,
                screeningId: null,
                formktp: {
                    isSearchKtp: false,
                    status_peserta: "transit",
                    ktp_no: "",
                    ktp_nama: "",
                    jenis_kelamin: "",
                    tempat_lahir: "",
                    tgl_lahir: "",
                    negara_id: 74,
                    prov_id: 0,
                    kab_id: 0,
                    kec_id: 0,
                    kel_id: 0,
                    dusun: "",
                    rtrw: "",
                    isWa: true,
                    wa_no: "",
                    isTelp: true,
                    telp_no: "",
                },
                formkontakcepat: {
                    isWa: true,
                    wa_no: "",
                    isTelp: true,
                    telp_no: "",
                },
                formnonktp: {
                    status_peserta: "transit",
                    jenis_identitas: "sim",
                    jenis_kelamin: "",
                    ktp_no: "",
                    ktp_nama: "",
                    tempat_lahir: "",
                    tgl_lahir: "",
                    negara_id: 74,
                    prov_id: 0,
                    kab_id: 0,
                    kec_id: 0,
                    kel_id: 0,
                    dusun: "",
                    rtrw: "",
                    isWa: true,
                    wa_no: "",
                    isTelp: true,
                    telp_no: "",
                },
                formdomisili: {
                    prov_id: 0,
                    kab_id: 0,
                    kec_id: 0,
                    kel_id: 0,
                    dusun: "",
                    rtrw: "",
                    alamat: "",
                    keluarga_nama: "",
                    keluarga_isWa: true,
                    keluarga_wa_no: "",
                    keluarga_isTelp: true,
                    keluarga_telp_no: "",
                },
                formkesehatan: {
                    status_kesehatan: "sehat",
                    demam: "tidak",
                    suhu: "",
                    batuk: "tidak",
                    sakit_tenggorokan: "tidak",
                    sakit_kepala: "tidak",
                    lemas: "tidak",
                    sesak_napas: "tidak",
                    status_kondisi: 1,
                    faskes: "",
                    tanggal: "",
                },
                formlain: {
                    tetangga_covid: "tidak",
                    catatan: ""
                },
                formriwayat: {
                    riwayat_no: "",
                    tanggal: "",
                    rute: "",
                    durasi: "",
                    jenis_transportasi: "umum",
                },
                riwayat: [],
                listktp: [{ sample: true }],
                listscreening: [{ sample: true }],
                historyscreening: []
            }
        default:
            return state

    }
}

export default screening