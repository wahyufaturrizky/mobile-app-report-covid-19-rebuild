import { Alert } from "react-native"
import moment from "moment"
import { post } from "../helpers/fetch"
import RNFetchBlob from "rn-fetch-blob"
import { baseApi } from "../helpers/url"

export const screening_setScreeningId = () => (dispatch, getState) => {
	const { root: { user } } = getState()
	const payload = `SCR.ID.${user.id}.${moment(new Date()).format("YYYYMMDD.HHmmss")}`
	dispatch({ type: "SET_SCREENING_ID", payload })
}

export const screening_getDataKtp = (ktp_no) => (dispatch, getState) => {
	// post(`getktp`, ).then(resktp => {
	// 	setIsSearchKtp(true)
	// 	if (searchktp !== "") {
	// 		if (resktp.NIK) {
	// 			const { PROP_NAME, KAB_NAME, KEC_NAME, KEL_NAME } = resktp.data
	// 			Alert.alert("Data ditemukan", "Selamat,\nData No. KTP peserta screening COVID19 berhasil ditemukan.\n\nSilahkan lanjutkan aktivitas screening.")
	// 		} else {				
	// 			Alert.alert("Data tidak ditemukan", "Data No. KTP peserta screening COVID19 tidak berhasil ditemukan,\n\nSilahkan lakukan pengisian data identitas peserta screening secara manual")
	// 		}
	// 	} else {
	// 		Alert.alert("Silahkan masukkan No. KTP dahulu.")
	// 	}

	// })
	// .catch(() => {
	// 	Alert.alert("Kesalahan","Respon server tidak valid.")
	// 	setIsSearchKtp(true)
	// })
	// DUMMY 
	const res = {
		"data": {
			"NO_KK": 3578030301080851,
			"NIK": 3578031104690001,
			"NAMA_LGKP": "HENDRI KUSUMAWARDHANA",
			"KAB_NAME": "KOTA SURABAYA",
			"AGAMA": "KRISTEN",
			"NAMA_LGKP_AYAH": "WARDANI TJIPTOWARDONO,DRS",
			"NO_RW": 6,
			"KEC_NAME": "RUNGKUT",
			"JENIS_PKRJN": "KARYAWAN SWASTA",
			"NO_RT": 2,
			"NO_KEL": 3578100003,
			"ALAMAT": "BARUK UTARA 4/7",
			"NO_KEC": 3578100,
			"TMPT_LHR": "TULUNGAGUNG",
			"PDDK_AKH": "STRATA-II",
			"NO_PROP": 35,
			"STATUS_KAWIN": "KAWIN",
			"NAMA_LGKP_IBU": "SETIATI",
			"PROP_NAME": "JAWA TIMUR",
			"GOL_DARAH": "A",
			"NO_KAB": 3578,
			"KEL_NAME": "KEDUNG BARUK",
			"JENIS_KLMIN": "Laki-Laki",
			"TGL_LHR": "1969-04-11"
		},
		"request_timestamp": null,
		"response_timestamp": "2020-03-31 23:30:43",
		"success": true
	}
	if (res.success) {
		Alert.alert("Data ditemukan", "Selamat,\nData No. KTP Warga screening COVID19 berhasil ditemukan.\n\nSilahkan lanjutkan aktivitas screening.")

		const { root: { provinsi } } = getState()
		const { NIK, NAMA_LGKP, JENIS_KLMIN, TMPT_LHR, TGL_LHR, ALAMAT, NO_RT, NO_RW, NO_PROP, NO_KAB, NO_KEC, NO_KEL } = res.data
		let payload = {
			isSearchKtp: true,
			ktp_no: `${NIK}`,
			ktp_nama: NAMA_LGKP,
			jenis_kelamin: JENIS_KLMIN,
			tempat_lahir: TMPT_LHR,
			tgl_lahir: moment(TGL_LHR, "YYYY-MM-DD").toDate(),
			dusun: ALAMAT,
			rtrw: `${NO_RT} / ${NO_RW}`,
			prov_id: 0,
			kab_id: 0,
			kec_id: 0,
			kel_id: 0,
		}
		const prov = provinsi.find(x => x.kd_prov === NO_PROP)
		if (prov) {
			payload["prov_id"] = prov.id
			dispatch({ type: "SET_FORM_KTP", payload })
			post(`http://ems-service.jatimprov.smartcovid19.com/api/get-kab`, { id: prov.id }).then(resKabupaten => {
				if (resKabupaten.success) {
					dispatch({ type: "SET_KABUPATEN", payload: resKabupaten.data })
					const kab = resKabupaten.data.find(x => x.kd_kota === NO_KAB)
					if (kab) {
						dispatch({ type: "HANDLE_FORM_KTP", payload: { name: "kab_id", value: kab.id } })
						post(`http://ems-service.jatimprov.smartcovid19.com/api/get-kec`, { id: kab.id }).then(resKecamatan => {
							if (resKecamatan.success) {
								dispatch({ type: "SET_KECAMATAN", payload: resKecamatan.data })
								const kec = resKecamatan.data.find(x => x.kd_kec === NO_KEC)
								if (kec) {
									dispatch({ type: "HANDLE_FORM_KTP", payload: { name: "kec_id", value: kec.id } })
									post(`http://ems-service.jatimprov.smartcovid19.com/api/get-kel`, { id: kec.id }).then(resKelurahan => {
										if (resKelurahan.success) {
											dispatch({ type: "SET_KELURAHAN", payload: resKelurahan.data })
											const kel = resKelurahan.data.find(x => x.kd_kel === NO_KEL)
											if (kel) {
												dispatch({ type: "HANDLE_FORM_KTP", payload: { name: "kel_id", value: kel.id } })
											}
										}
									})
								}
							}
						})
					}

				}
			})
		}
	} else {
		Alert.alert("Data tidak ditemukan", "Data No. KTP Warga screening COVID19 tidak berhasil ditemukan,\n\nSilahkan lakukan pengisian data identitas Warga screening secara manual")
	}
}


export const screening_setFormKtp = payload => ({ type: "SET_FORM_KTP", payload })
export const screening_reset = () => ({ type: "RESET" })
export const screening_setRiwayat = payload => ({ type: "SET_RIWAYAT", payload })

export const screening_addListKtp = payload => ({ type: "ADD_LISTKTP", payload })
export const screening_addListScreening = payload => ({ type: "ADD_LISTSCREENING", payload })

export const screening_removeListKtp = payload => ({ type: "REMOVE_LISTKTP", payload })
export const screening_removeListScreening = payload => ({ type: "REMOVE_LISTSCREENING", payload })

export const screening_handleFormKtp = (name, value) => ({ type: "HANDLE_FORM_KTP", payload: { name, value } })
export const screening_handleFormNonKtp = (name, value) => ({ type: "HANDLE_FORM_NON_KTP", payload: { name, value } })
export const screening_handleFormKontakCepat = (name, value) => ({ type: "HANDLE_FORM_KONTAK_CEPAT", payload: { name, value } })
export const screening_handleFormDomisili = (name, value) => ({ type: "HANDLE_FORM_DOMISILI", payload: { name, value } })
export const screening_handleFormKesehatan = (name, value) => (dispatch, getState) => {
	const { screening: { formkesehatan } } = getState()
	let status_kondisi = 0
	let tanggal = moment(new Date()).format("YYYY-MM-DD")
	if (formkesehatan.lemas === "ya" || formkesehatan.sesak_napas === "ya") {
			status_kondisi = 3			
	} else if (
			formkesehatan.demam === "ya" || 
			formkesehatan.batuk === "ya" ||
			formkesehatan.sakit_tenggorokan === "ya" ||
			formkesehatan.sakit_kepala === "ya"
	) {
			tanggal = moment(new Date()).add(1, "day").format("YYYY-MM-DD")
			status_kondisi = 2
	} else {
			status_kondisi = 1
	} 
	dispatch({ type: "HANDLE_FORM_KESEHATAN", payload: { name, value, status_kondisi, tanggal } })
}
export const screening_handleFormLain = (name, value) => ({ type: "HANDLE_FORM_LAIN", payload: { name, value } })
export const screening_handleFormRiwayat = (name, value) => ({ type: "HANDLE_FORM_RIWAYAT", payload: { name, value } })
export const screening_history = payload => ({ type: "HISTORY_LISTSCREENING", payload })

export const screening_saveScreening = (navigation) => async (dispatch, getState) => {	
	const { root: { user, negara, provinsi, kabupaten, kecamatan, kelurahan }, screening: { formktp, formnonktp, listktp, listscreening } } = getState()
	if (user) {	
		const negaraName = (formktp.negara_id !== 74) ? negara.find(x => x.id === formktp.negara_id)["nama"] : ((formnonktp.negara_id !== 74) ? negara.find(x => x.id === formnonktp.negara_id)["nama"] : "INDONESIA")
		const provName = (formktp.prov_id) ? provinsi.find(x => x.id === formktp.prov_id)["nama"] : ((formnonktp.prov_id) ? provinsi.find(x => x.id === formnonktp.prov_id)["nama"] : "-")
		const kabName = (formktp.kab_id) ? kabupaten.find(x => x.id === formktp.kab_id)["nama"] : ((formnonktp.kab_id) ? kabupaten.find(x => x.id === formnonktp.kab_id)["nama"] : "-")
		const kecName = (formktp.kec_id) ? kecamatan.find(x => x.id === formktp.kec_id)["nama"] : ((formnonktp.kec_id) ? kecamatan.find(x => x.id === formnonktp.kec_id)["nama"] : "-")
		const kelName = (formktp.kel_id) ? kelurahan.find(x => x.id === formktp.kel_id)["nama"] : ((formnonktp.kel_id) ? kelurahan.find(x => x.id === formnonktp.kel_id)["nama"] : "-")

		const dataSend = {
			userid: user.id,
			status_covid: 0,
			no_identitas: formktp.ktp_no !== "" ? formktp.ktp_no : (formnonktp.ktp_no !== "" ? formnonktp.ktp_no : "-"),
			nama: formktp.ktp_nama !== "" ? formktp.ktp_nama : (formnonktp.ktp_nama !== "" ? formnonktp.ktp_nama : "-"),
			jenis_kelamin: formktp.jenis_kelamin !== "" ? formktp.jenis_kelamin : (formnonktp.jenis_kelamin !== "" ? formnonktp.jenis_kelamin : "-"),
			negara: negaraName,
			provinsi: provName,
			kota_kabupaten: kabName,
			kecamatan: kecName,
			desa_kelurahan: kelName,
			dusun: formktp.dusun !== "" ? formktp.dusun : (formnonktp.dusun !== "" ? formnonktp.dusun : "-"),
			rt: formktp.rtrw !== "" ? formktp.rtrw : (formnonktp.rtrw !== "" ? formnonktp.rtrw : "-"),
			rw: formktp.rtrw !== "" ? formktp.rtrw : (formnonktp.rtrw !== "" ? formnonktp.rtrw : "-"),
		}
	
		const dataKtp = listktp.filter(x => x.base64).map((data, i) => ({ name: 'identitas', filename: `identitas-${user.id}-${i+1}-${moment(new Date()).format("YYYYMMDD-HHmmss")}.jpg`, data: data.base64 }))
		const dataScreening = listscreening.filter(x => x.base64).map((data, i) => ({ name: 'screening', filename: `screening-${user.id}-${i+1}-${moment(new Date()).format("YYYYMMDD-HHmmss")}.jpg`, data: data.base64 }))
		const dataWrapToSend = [ ...dataKtp, ...dataScreening, { name: 'form', data: JSON.stringify(dataSend) } ]
		return await RNFetchBlob.fetch('POST', baseApi + '/screening', {
			'Content-Type' : 'multipart/form-data',
		}, dataWrapToSend)
		.then(response => {			
			const res = JSON.parse(response.data)
			if (res.status === "success") {
				Alert.alert("Berhasil", "Berhasil menyimpan data screening.")
				dispatch({ type: "RESET" })
				navigation.navigate("Dashboard")
			} else {			
				Alert.alert("Gagal", "Gagal menyimpan data screening, mohon ulangi.")
			}
		})
		.catch(err => {
			Alert.alert("Kesalahan", "Mohon ulangi proses simpan (" + err + ")")
		})
	} else {
		Alert.alert("Expired", "Gagal menyimpan data screening, mohon ulangi..")
	}
}